package com.manoj.skafold.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkafoldSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkafoldSpringBootApplication.class, args);
	}

}
